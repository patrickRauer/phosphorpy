Phosphorpy package
==================

Subpackages
-----------

.. toctree::

   Phosphorpy.config
   Phosphorpy.core
   Phosphorpy.data
   Phosphorpy.external
   Phosphorpy.fitting
   Phosphorpy.report

Module contents
---------------

.. automodule:: Phosphorpy
   :members:
   :undoc-members:
   :show-inheritance:
